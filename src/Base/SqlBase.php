<?php

namespace Phr\Sqlbridge\Base;

use Phr\Sqlbridge\SqlException;

/**
 * @abstract SqlBase
 * @abstract DatabaseConfig
 * @see DatabaseConfig
 * 
 * Basic sql methods
 */
abstract class SqlBase extends DatabaseConfig
{   
    public static $connect;
    /**
     * @access public
     * 
     * @static
     * 
     * @method connect
     * @return bool
     * 
     * @throws SqlException
     */
    public static function connect(): bool
    {   
        mysqli_report(MYSQLI_REPORT_STRICT);
        self::$connect = new \mysqli(self::$host, self::$user, self::$pass,self::$shema,self::$port);       
        self::$connect->set_charset(self::CHARSET);
        return  (!mysqli_connect_errno() ? true: throw new SqlException( mysqli_connect_errno() ) );
    }

    /**
     * @method stmt
     * @var string query
     */
    public static function stmt( string $_query )
    {   
        if( !$_query ) new SqlException("missing query string");
        return self::$connect->prepare($_query);
    }

    /**
     * @method close
     */
    public static function close(): bool
    {
        return self::$connect->close();
    }
}