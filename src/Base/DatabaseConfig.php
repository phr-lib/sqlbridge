<?php

namespace Phr\Sqlbridge\Base;

use Phr\Sqlbridge\SqlSettings;
use Phr\Sqlbridge\SqlException;

/**
 * @abstract DatabaseConfig
 * 
 * Configures basic sql requrements.
 * 
 */
abstract class DatabaseConfig 
{   
    /**
     * @access protected
     * 
     */
    protected const CHARSET = "utf8";

    /**
     * @var static
     * @var string host
     */
    protected static string $host;

    /**
     * @var int port
     */
    protected static int $port;

    /**
     * @var string shema
     */
    protected static string $shema;

    /**
     * @var string user
     */
    protected static string $user;   

    /**
     * @var string pass
     */
    protected static string $pass;

    /**
     * @var static
     * @var connect
     */
    protected static $connect;    

    /// CONSTRUCT ***

    public function __construct( SqlSettings $_settings )
    {
        self::configure( $_settings );
    }

    /**
     * @access private
     * 
     * @method configure
     * @var SqlSettings
     * 
     * Configures basic sql requrements.
     */
    private static function configure( SqlSettings $_settings ): void 
    {   
        if( !$_settings->host ) throw new SqlException("missing host");
        self::$host = $_settings->host;

        if( !$_settings->port ) throw new SqlException("missing port");
        self::$port = $_settings->port;

        if( !$_settings->shema ) throw new SqlException("missing shema");
        self::$shema = $_settings->shema;

        if( !$_settings->username ) throw new SqlException("missing user");
        self::$user = $_settings->username;

        if( !$_settings->password ) throw new SqlException("missing password");
        self::$pass = $_settings->password;
    }


}