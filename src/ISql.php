<?php

namespace Phr\Sqlbridge;

/**
 * PHP 8.2 or above
 * 
 * @category sql bridge and pgr migration
 * 
 * @author Grega Lipovšček
 * @license https://lab.ortus.si
 * @link grega.lipovscek@ortus.si
 * 
 * @see Sql
 * 
 */
interface ISql 
{   
    /**
     * @method fetches all fields in table,
     * if there is no result returns null.
     * @param string fetching table
     * @param string|null where --- aditional fetching parameter
     * @return array|null array of table rows
     * @throws SqlException
     */
    public function fetch(string $_fetching_table, array|null $_where = null): array|null;
    /**
     * @method basic mysqlli query
     * @param string query
     * @return mixed result
     */
    public function query( string $_query ): mixed;
    /**
     * @method prepares sql query
     * @param string query (int)(??)
     * @return stmt
     */
    public static function prepare( string $_query );
    /**
     * @method executes query, close stmt and
     * closes sql connection.
     * @param stmt
     * @return bool true inf connections is executed and
     * all connection to database closed.
     */
    public static function execute( $_stmt_query ): bool;
    /**
     * @method closes stmt and database connection.
     * @param stmt
     * @return bool true if connecion is closed.
     */
    public static function kill( $_stmt_query ): bool;
}