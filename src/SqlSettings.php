<?php

namespace Phr\Sqlbridge;

use Phr\Sqlbridge\SqlException;

/**
 * SQL settings
 */
class SqlSettings 
{
    public string $host;

    public int $port;

    public string $username;

    public string $password;

    public string $shema;

    public function __construct( string $_host
        ,string $_username
        ,string $_password
        ,string $_shema
        ,int $_port = 3306 
    ){
        if(!$_host) throw new SqlException('host', 5800001);
        $this->host = $_host;
        if(!$_username) throw new SqlException('username', 5800001);
        $this->username = $_username;
        if(!$_password) throw new SqlException('password', 5800001);
        $this->password = $_password;
        if(!$_shema) throw new SqlException('shema', 5800001);
        $this->shema = $_shema;
        if(!$_port) throw new SqlException('port', 5800001);
        $this->port = $_port;
    }
}