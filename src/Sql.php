<?php

namespace Phr\Sqlbridge;

use Phr\Sqlbridge\SqlException;

/**
 * @see interface ISql
 */
final class Sql extends Base\SqlBase implements ISql
{   
    public function query( string $_query ): mixed
    {   
        try{
            
            if(parent::connect()) $result = self::$connect->query($_query);
            self::close();
            if($result == false) throw new SqlException("DB executing", 5801005); 
            else return $result; 
            
        }catch(\mysqli_sql_exception $error)
        {   
            throw new SqlException($error->getMessage(), 5801002);
        }
        return false;    
    }
    public static function prepare( string $_query )
    {   
        parent::connect();
        return parent::stmt( $_query );
    }
    public static function execute( $_stmt_query ): bool
    {   
        $_stmt_query->execute();
        $_stmt_query->close();
        return parent::close();
    }
    public static function kill( $_stmt_query ): bool
    {   
        $_stmt_query->close();
        return parent::close();
    }
    public function fetch(string $_fetching_table, array|null $_where = null): array|null
    {   
        if(!$_fetching_table) throw new SqlException('missing table', 5801009);
        if($_where != null) $q = "SELECT * FROM {$_fetching_table} WHERE {$_where[0]}='{$_where[1]}'";
        else $q = "SELECT * FROM {$_fetching_table}";
        if(parent::connect())
        $result = mysqli_query(self::$connect, $q);
        if($result == null) return null;
        $resultNumber =  mysqli_num_rows($result);
        if($resultNumber < 1) return null;
        $results = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $result->free_result();
        parent::close();
        return $results;
    }
    public function delete(string $_table, array $_where): bool
    {
        $q = "DELETE FROM {$_table} WHERE {$_where[0]}='{$_where[1]}'";
        if(parent::connect())
        return mysqli_query(self::$connect, $q);
    }
    
}