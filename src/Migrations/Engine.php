<?php

namespace Phr\Sqlbridge\Migrations;

use Phr\Sqlbridge\Entity;
use Phr\Sqlbridge\SqlSettings;
use Phr\Sqlbridge\SqlException;
use Phr\Sqlbridge\Sql;


/**
 * @abstract Engine - Extends phr migration 
 * class
 * 
 * @see project Migrations
 * 
 */
abstract class Engine extends BaseEntity
{   
    protected static $sql;

    protected static $database;

    public function __construct(SqlSettings $_settings)
    {
        self::$sql = new Sql($_settings);
        self::$database = $_settings->shema;
    }

    public function parser(array $_parser)
    {   
        for($i=0; $i<count($_parser); $i++)
        {
            $entity = $_parser[$i][0];
            
            $table = self::getTable(get_class($entity));
            
            $sql = $entity->parse($_parser[$i][1]);
            $sqlCommand =  self::create([self::$database, $table]) . $sql;
            $this->sendToDatabase($sqlCommand.' ENGINE = InnoDB');

        }        
    }
    private function sendToDatabase(string $_sql_comand)
    {
        try{
            self::$sql->query($_sql_comand); 
        }catch(SqlException $engineError)
        {
            throw new SqlException('Migrations::'.$engineError->getMessage(), $engineError->getCode());
        }     
    }
    
}