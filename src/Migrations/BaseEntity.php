<?php

namespace Phr\Sqlbridge\Migrations;


/**
 * Sql and class types
 * 
 */
enum Type
{
    case VARCHAR;
    case INTIGER;
    case PRIMARY;
    case BOOL;
    case FLOAT;
    case UNIQUE;
    case NOT_NULL;
    case NULL;
    case DATETIME;
    case NOW;
    case UPDATE;







    public function sql(): string 
    {
        return match($this)
        {
            Type::VARCHAR => 'VARCHAR',
            Type::INTIGER => 'INT',
            Type::BOOL => 'BOOLEAN',
            Type::FLOAT => 'FLOAT',
            Type::PRIMARY => 'PRIMARY KEY',
            Type::UNIQUE => 'UNIQUE',
            Type::NOT_NULL => 'NOT NULL',
            Type::NULL => 'NULL',
            Type::DATETIME => 'DATETIME',
            Type::NOW => 'DEFAULT CURRENT_TIMESTAMP',
            Type::UPDATE => 'on update CURRENT_TIMESTAMP'




        };
    }
}
/**
 * @abstract BaseEntity
 * 
 * @see Engine
 * @see Entity
 * 
 */
abstract class BaseEntity 
{   
    /**
     * @access protected
     * 
     * @var sql 
     * 
     */
    protected const CREATE = 'CREATE TABLE ';
    protected const INSERT = 'INSERT INTO ';
    protected const UPDATE = 'UPDATE ';
    protected const DELETE = 'DELETE ';
    protected const VALUES = 'VALUES';
    protected const SET = ' SET ';
    protected const WHERE = ' WHERE ';





    /**
     * @var migrationEngine constatnts
     */
    protected const VARCHAR = 'VARCHAR';
    protected const INTIGER = 5;
    protected const BOOL = true;
    protected const FLOAT = 1.1;
    protected const DATETIME = 'DATETIME';

    /**
     * @var delimiters 
     */
    protected const D = "`";
    protected const S = " ";
    protected const O = "(";
    protected const C = ")";
    protected const SEP = ", ";
    protected const COM = ".";
    protected const HY = "\"";

    public function insert(string $_table): string
    {   
        $sqlVars = self::O;
        $sqlValues = self::VALUES.self::O;
        
        $varNum =  count(get_class_vars(get_class($this)));
        
        $i = 1;
        foreach($this as $var => $values)
        {
            switch(self::getTable(get_debug_type($values)))
            {   
                case 'DateTime': break;
                case 'Uuid':  
                            if($i > 1) $sqlVars .= self::SEP;
                            $sqlVars .= $var;
                            if($i > 1) $sqlValues .= self::SEP;
                            $sqlValues .= self::HY.$values->value.self::HY; 
                
                break;
                default:    if($i > 1) $sqlVars .= self::SEP;
                            $sqlVars .= $var;
                            if($i > 1) $sqlValues .= self::SEP;
                            $sqlValues .= self::HY.$values.self::HY; 
                        
                break;
            }
            
            
            $i++;
        }
        $sqlVars .= self::C;
        $sqlValues .= self::C;
        return self::insertInto($_table).$sqlVars.$sqlValues;
    }
    public function update(string $_table, array $_where)
    {
        $sql = "";
        $i = 1;
        foreach($this as $var => $values)
        {   
            switch(self::getTable(get_debug_type($values)))
            {   
                
                case 'DateTime':$i--; break;
                case 'Uuid': $i--; break;
                default:if($i > 1) $sql .= self::SEP; 
                        $sql .= $var.'='.self::HY.$values.self::HY; break;
            }
            $i++;
        }
        return self::updateTable($_table).$sql.self::where($_where);
    }
    
    public static function getTable(string $_entity)
    {
        $ex = explode('\\', $_entity);
        return array_pop($ex);
    }
    /**
     * @method parse / parsing classes into
     * sql command
     * 
     * @param array class parser
     * 
     * @return string sql key parse
     * string
     * 
     */
    protected function parse(array $_parser): string
    {   
        $i = 0;
        $last = count($_parser) -1;
        $sql = self::O;
        $spc = "";
        foreach($this as $var => $value)
        {   
            $sql .= self::D;
            $sql .= $var;
            $sql .= self::D;
            $sql .= self::S;
            
            switch (get_debug_type($value)) 
            {   
                case 'int': $sql .= Type::INTIGER->sql(); break;
                case 'float': $sql .= Type::FLOAT->sql(); break;
                case 'bool': $sql .= Type::BOOL->sql(); break;
                case 'DateTime': $sql .= Type::DATETIME->sql(); break;

                
                default: $sql .=  Type::VARCHAR->sql();; break;
            }
            
            switch (get_debug_type($value)) 
            {   
                case 'bool': ; break;
                case 'DateTime': ; break;
                
                default: $sql .= self::length($_parser[$i][0]); break;
            }
                         
            $sql .= self::S;

            if(isset($_parser[$i][1]))
            {   
                if($_parser[$i][1] == null) $sql .= Type::NULL->sql();
                else $sql .= Type::NOT_NULL->sql();
                
            }else 

            switch (get_debug_type($value)) 
            {
                case 'DateTime': switch($var)
                                    {
                                        case 'created': $sql .= ' '.Type::NOW->sql();
                                        case 'updated': $sql .= ' '.Type::UPDATE->sql();
                                    }
                    break;
                default: $sql .= Type::NOT_NULL->sql(); break;
            }
            
            if(isset($_parser[$i][2]))
            {   
                $spc .=  self::wrapspc($_parser[$i][2], $var);
            }

            # End parse
            if($i != $last) $sql .= self::SEP;
            $sql .= self::S;
            $i++;
        }
        
        $sql .= $spc;
        $sql .= self::C;
        return $sql;
    }
    protected static function create(array $_dbTable)
    {
        return self::CREATE.self::D . $_dbTable[0]. self::D. self::COM .self::D.$_dbTable[1].self::D.self::S;
    }
    
    /**
     * @access private
     */
    private static function length(int|float $_length): string
    {
        return self::O . $_length . self::C;
    }
    private static function wrapspc(string $_spc, string $_var): string 
    {
        return self::SEP . $_spc . self::O. self::D. $_var . self::D. self::C;
    }
    private static function insertInto(string $_table): string 
    {
        return self::INSERT.$_table.self::S;
    }
    private static function updateTable(string $_table): string 
    {
        return self::UPDATE.$_table.self::S.self::SET;
    }
    private static function where(array $_where): string 
    {
        return self::WHERE.$_where[0].'='.self::HY.$_where[1].self::HY;
    }
    
}