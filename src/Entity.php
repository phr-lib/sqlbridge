<?php

namespace Phr\Sqlbridge;

use Phr\Sqlbridge\Migrations\BaseEntity;

/**
 * @abstract class
 * 
 * Entity model
 * 
 * Used to extentiate phr
 * migration class in web solution.
 * 
 */
abstract class Entity extends BaseEntity implements IEntity{}