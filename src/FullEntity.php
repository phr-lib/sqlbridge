<?php

namespace Phr\Sqlbridge;

use Phr\Sqlbridge\Migrations\BaseEntity;
use Phr\Sqlbridge\Tools\Uuid;

/**
 * @abstract class
 * 
 * Entity model
 * 
 * Used to extentiate phr
 * migration class in web solution.
 * 
 */
abstract class FullEntity extends Entity implements IEntity
{   
    public Uuid $uuid;

    public \DateTime $created;

    public \DateTime $updated;

    public \DateTime $deleted;



    public function __construct(
    ){  
        $this->uuid = new Uuid;
        $this->created = new \DateTime('now');
        $this->updated = new \DateTime('now');
        $this->deleted = new \DateTime('now');

    }

}