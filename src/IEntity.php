<?php

namespace Phr\Sqlbridge;

/**
 * PHP 8.2 or above
 * 
 * @category sql entity
 * 
 * @author Grega Lipovšček
 * @license https://lab.ortus.si
 * @link grega.lipovscek@ortus.si
 * 
 * @see Entity
 * 
 */
interface IEntity 
{   
    /**
     * @method create sql query
     * 
     * @param string table
     * @return string sql query
     */
    public function insert(string $_table): string;
}